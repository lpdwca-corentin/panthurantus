<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<title><?php print($this->get_title()); ?> &ndash; Panthurantus</title>
	<meta name="description" content="<?php print($this->get_description()); ?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="/static/css/design.css" />
	<link rel="icon" type="image/png" href="/static/img/icon.png" />
</head>
<body>
	<div id="wrapper">
