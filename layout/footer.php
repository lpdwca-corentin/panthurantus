	</div>
	<footer>
		<p>
			<a href="/">Panthurantus</a> 2020 - Tous droits réservés - <a href="/cgu/">CGU</a>
		</p>
		<p>
			<i>Une application de Corentin Bettiol</i>
		</p>
	</footer>
	<?php if(isset($this->msg)) {
		print('<div class="msg '. $this->msg[0] .'_msg">'. $this->msg[1] .'</div>');
	} ?>
</body>
</html>
