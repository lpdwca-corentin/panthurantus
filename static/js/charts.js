function createChartStackedBars(data){
	var ctx = document.getElementById('stackedBars').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
			responsive: true,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			},
			title: {
				display: true,
				text: "Évolution des entrées/sorties d'argent par mois"
			}
		}
	});
}

function createCategories(data){
	var ctx = document.getElementById('barsCategories').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
			maintainAspectRatio: false,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: "Évolution des catégories par mois"
			}
		}
	});
}

function createCategoriesNb(data){
	var ctx = document.getElementById('linesCategories').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
			maintainAspectRatio: false,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: "Nombre d'opération mensuel"
			},
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
}

function createPayments(data){
	var ctx = document.getElementById('barsPayments').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
			maintainAspectRatio: false,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: "Évolution de l'utilisation des moyens de paiement par mois"
			}
		}
	});
}

function getData(type, duration, funcname){

	fetch(
		"/api/getData/" + type + "/" + duration + "/",
		{
			method: 'GET',
			credentials: 'include'
		}
	).then(
		response => response.json()
	).then(function(json) {
		window[funcname](json);
	});

}


window.onload = function() {
	getData("amount", "all", "createChartStackedBars");
	getData("categories", "all", "createCategories");
	getData("payments", "all", "createPayments");
	getData("categories_nb", "all", "createCategoriesNb");
};
