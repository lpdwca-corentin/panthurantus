<?php

class SettingsPageManager extends Base
{
	private $title;
	private $description;
	private $h1;

	function __construct($title, $description, $h1)
	{
		parent::__construct();
		$this->title = htmlspecialchars($title);
		$this->description = htmlspecialchars($description);
		$this->h1 = htmlspecialchars($h1);
	}

	public function name(){
		return 0;
	}

	public function render(){
		include("layout/head.php");
		include("view/public/menu.php");
		include("view/public/settings.php");
		include("layout/footer.php");
	}

	public function get_title(){
		return $this->title;
	}
	public function get_description(){
		return $this->description;
	}
	public function get_h1(){
		return $this->h1;
	}
}
