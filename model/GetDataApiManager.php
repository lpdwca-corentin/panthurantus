<?php
/*
Class that handle all graph-related data, and then return plain text json content (because js can load json even if header doesn't contain json but only plain text)

*/
class GetDataApiManager extends Base
{

	function __construct()
	{
		parent::__construct();
	}

	public function getData($type, $duration){

		switch ($type) {
			case 'amount':
				$this->return_amount_json();
				break;

			case 'categories':
				$this->return_categories($duration);
				break;

			case 'categories_nb':
				$this->return_categories_lines($duration);
				break;

			case 'payments':
				$this->return_payments($duration);
				break;

			default:
				header('Location: /404/');
				break;
		}


	}

	public function return_categories($duration){

		$req = self::$db->prepare('
			SELECT
			COUNT(operation.id) as nb,
			category.name as name,
			TRUNCATE(SUM(operation.amount), 2) as total,
			MONTH(operation.time) as month,
			YEAR(operation.time) as year
			FROM '. dbName .'.operation
			LEFT JOIN '. dbName .'.category ON operation.category = category.id
			WHERE operation.user = :user
			GROUP BY operation.category, YEAR(operation.time), MONTH(operation.time)
			ORDER BY YEAR(operation.time), MONTH(operation.time)
		');
		$req->execute([
			':user' => $_SESSION['id']
		]);
		$data = $req->fetchAll();

		$names = self::$db->query('SELECT * FROM '. dbName .'.category');
		$names = $names->fetchAll();

		$datasets = [];
		$labels = [];
		$bufferData = array_fill(0, count($names), array_fill(0, count($data), 0));
		$bufferMonth = -1;
		$bufferYear = -1;
		$bg = [
			"#003366",
			"#3366cc",
			"#666699",
			"#6600ff",
			"#cc00ff",
			"#ff66ff",
			"#cc0000",
			"#ff6600",
			"#ffcc66",
			"#ccff66",
			"#ffff00",
			"#999966",
			"#66ff33",
			"#66ff99",
			"#009900",
			"#006600",
			"#00ffcc"
		];

		$month = 0;
		// for each different category for each month
		for($i = 0; $i<count($data); $i++){
			// if we are changing month
			if($bufferMonth != $data[$i]['month'] || ($bufferMonth == $data[$i]['month'] && $bufferYear != $data[$i]['year'])){
				// add new month in labels
				array_push($labels, ucfirst(strftime("%B", mktime(20, 0, 0, $data[$i]['month'], 1, $data[$i]['year']))) ." ". $data[$i]['year']);
				// update date to verify
				$bufferMonth = $data[$i]['month'];
				$bufferYear = $data[$i]['year'];
				if($i != 0)
					$month++;

			}
			// for each category name
			for($j = 0; $j<count($names); $j++){
				// if this is the name of the current category for this month
				if($names[$j]['name'] == $data[$i]['name']){
					// put the data in the current dataset
					$bufferData[$j][$month] = (int)$data[$i]['total'];
					$color = $bg[$j];
				}
			}

		}
		// for each category
		for($i = 0; $i<count($names); $i++){
			// create new dataset from bufferData, hide empty datasets
			if(array_sum($bufferData[$i]) == 0)
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i], "hidden" => 'true']);
			else
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i]]);
		}
		$data = [
			"labels" => $labels,
			"datasets" => $datasets
		];

		print_r(json_encode($data));

	}


	public function return_categories_lines($duration){

		$req = self::$db->prepare('
			SELECT
			COUNT(operation.id) as nb,
			category.name as name,
			TRUNCATE(SUM(operation.amount), 2) as total,
			MONTH(operation.time) as month,
			YEAR(operation.time) as year
			FROM '. dbName .'.operation
			LEFT JOIN '. dbName .'.category ON operation.category = category.id
			WHERE operation.user = :user
			GROUP BY operation.category, YEAR(operation.time), MONTH(operation.time)
			ORDER BY YEAR(operation.time), MONTH(operation.time)
		');
		$req->execute([
			':user' => $_SESSION['id']
		]);
		$data = $req->fetchAll();

		$names = self::$db->query('SELECT * FROM '. dbName .'.category');
		$names = $names->fetchAll();

		$datasets = [];
		$labels = [];
		$bufferData = array_fill(0, count($names), array_fill(0, count($data), 0));
		$bufferMonth = -1;
		$bufferYear = -1;
		$bg = [
			"#003366",
			"#3366cc",
			"#666699",
			"#6600ff",
			"#cc00ff",
			"#ff66ff",
			"#cc0000",
			"#ff6600",
			"#ffcc66",
			"#ccff66",
			"#ffff00",
			"#999966",
			"#66ff33",
			"#66ff99",
			"#009900",
			"#006600",
			"#00ffcc"
		];

		$month = 0;
		// for each different category for each month
		for($i = 0; $i<count($data); $i++){
			// if we are changing month
			if($bufferMonth != $data[$i]['month'] || ($bufferMonth == $data[$i]['month'] && $bufferYear != $data[$i]['year'])){
				// add new month in labels
				array_push($labels, ucfirst(strftime("%B", mktime(20, 0, 0, $data[$i]['month'], 1, $data[$i]['year']))) ." ". $data[$i]['year']);
				// update date to verify
				$bufferMonth = $data[$i]['month'];
				$bufferYear = $data[$i]['year'];
				if($i != 0)
					$month++;

			}
			// for each category name
			for($j = 0; $j<count($names); $j++){
				// if this is the name of the current category for this month
				if($names[$j]['name'] == $data[$i]['name']){
					// put the data in the current dataset
					$bufferData[$j][$month] = (int)$data[$i]['nb'];
					$color = $bg[$j];
				}
			}

		}
		// for each category
		for($i = 0; $i<count($names); $i++){
			// create new dataset from bufferData, hide empty datasets
			if(array_sum($bufferData[$i]) == 0)
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i], "hidden" => 'true', "fill" => 'false']);
			else
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i], "fill" => 'false']);
		}
		$data = [
			"labels" => $labels,
			"datasets" => $datasets
		];

		print_r(json_encode($data));

	}

	public function return_payments($duration){

		$req = self::$db->prepare('
			SELECT
			COUNT(operation.id) as nb,
			payment.name as name,
			TRUNCATE(SUM(operation.amount), 2) as total,
			MONTH(operation.time) as month,
			YEAR(operation.time) as year
			FROM '. dbName .'.operation
			LEFT JOIN '. dbName .'.payment ON operation.payment = payment.id
			WHERE operation.user = :user
			GROUP BY operation.payment, YEAR(operation.time), MONTH(operation.time)
			ORDER BY YEAR(operation.time), MONTH(operation.time)
		');
		$req->execute([
			':user' => $_SESSION['id']
		]);
		$data = $req->fetchAll();

		$names = self::$db->query('SELECT * FROM '. dbName .'.payment');
		$names = $names->fetchAll();

		$datasets = [];
		$labels = [];
		$bufferData = array_fill(0, count($names), array_fill(0, count($data), 0));
		$bufferMonth = -1;
		$bufferYear = -1;
		$bg = [
			"#003366",
			"#3366cc",
			"#666699",
			"#6600ff"
		];

		$month = 0;
		// for each different category for each month
		for($i = 0; $i<count($data); $i++){
			// if we are changing month
			if($bufferMonth != $data[$i]['month'] || ($bufferMonth == $data[$i]['month'] && $bufferYear != $data[$i]['year'])){
				// add new month in labels
				array_push($labels, ucfirst(strftime("%B", mktime(20, 0, 0, $data[$i]['month'], 1, $data[$i]['year']))) ." ". $data[$i]['year']);
				// update date to verify
				$bufferMonth = $data[$i]['month'];
				$bufferYear = $data[$i]['year'];
				if($i != 0)
					$month++;

			}
			// for each category name
			for($j = 0; $j<count($names); $j++){
				// if this is the name of the current category for this month
				if($names[$j]['name'] == $data[$i]['name']){
					// put the data in the current dataset
					$bufferData[$j][$month] = (int)$data[$i]['total'];
					$color = $bg[$j];
				}
			}

		}
		// for each payment
		for($i = 0; $i<count($names); $i++){
			// create new dataset from bufferData
			if(array_sum($bufferData[$i]) == 0)
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i], "hidden" => 'true']);
			else
				array_push($datasets, ["label" => $names[$i]['name'], "data" => $bufferData[$i], "backgroundColor" => $bg[$i]]);
		}
		$data = [
			"labels" => $labels,
			"datasets" => $datasets
		];

		print_r(json_encode($data));

	}


	public function return_amount_json(){

		$req = self::$db->prepare('
			SELECT
			TRUNCATE(SUM(case when amount>0 then amount else 0 end), 2) as positive,
			TRUNCATE(SUM(case when amount<0 then amount else 0 end), 2) as negative,
			MONTH(time) as month,
			YEAR(time) as year
			FROM '. dbName .'.operation
			WHERE user = :user
			GROUP BY YEAR(time), MONTH(time)
		');
		$req->execute([':user' => $_SESSION['id']]);
		$data = $req->fetchAll();


		$labels = [];
		for($i = 0; $i<count($data); $i++){
			$month = ucfirst(strftime("%B", mktime(20, 0, 0, $data[$i]['month'], 1, $data[$i]['year'])));
			array_push($labels, $month ." ". $data[$i]['year']);
		}

		$negative = [];
		$positive = [];
		$total = [];
		for($i = 0; $i<count($data); $i++){
			array_push($negative, $data[$i]['negative']);
			array_push($positive, $data[$i]['positive']);
			array_push($total, number_format($data[$i]['negative'] + $data[$i]['positive'], 2));
		}

		$dataset = [
			"labels" => $labels,
			"datasets" =>
			[
				[
					"label" => "Solde du mois",
					"backgroundColor" => "#aaf",
					"data" => $total,
					"type" => "line",
					"fill" => "false"
				],
				[
					"label" => "Entrées d'argent",
					"backgroundColor" => "#afa",
					"data" => $positive
				],
				[
					"label" => "Sorties d'argent",
					"backgroundColor" => "#faa",
					"data" => $negative
				]
			]
		];

		print_r(json_encode($dataset));
	}

}
