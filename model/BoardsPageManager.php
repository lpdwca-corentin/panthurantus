<?php

class BoardsPageManager extends Base
{
	private $title;
	private $description;
	private $h1;

	function __construct($title, $description, $h1)
	{
		parent::__construct();
		if(isset($_SESSION['year']))
			$this->year = $_SESSION['year'];
		else
			$this->year = date('Y');
		if(isset($_SESSION['month']))
			$this->month = $_SESSION['month'];
		else
			$this->month = date('m');


		$this->check_login();
		$this->title = htmlspecialchars($title);
		$this->h1 = htmlspecialchars($h1);
		$this->description = htmlspecialchars($description);
	}

	public function name(){
		return 0;
	}

	public function render(){
		include("layout/head.php");
		include("view/public/menu.php");

		$_GET['action'] ? $_GET['action'] : "default";
		switch ($_GET['action']) {
			case 'recent':
				$this->h1 = "Opérations récentes";
				include("view/public/boards_recent.php");
				break;

			case 'added':
				$this->h1 = "Dernières opérations ajoutées";
				include("view/public/boards_added.php");
				break;

			case 'year':
				$this->h1 = "Opérations de ". $this->year;
				include("view/public/boards_year.php");
				break;

			case 'month':
				$this->h1 = "Opérations du mois ". $this->get_correct_month_name() ." ". $this->year;
				include("view/public/boards_month.php");
				break;

			default:
				include("view/public/boards.php");
				break;
		}
		include("layout/footer.php");
	}

	public function get_title(){
		return $this->title;
	}
	public function get_description(){
		return $this->description;
	}
	public function get_h1(){
		return $this->h1;
	}


	public function print_last_transactions($type, $limit){

		switch ($type) {
			case 'added':
				$last_transactions = $this->get_last_transactions_added($limit);
				break;

			case 'recent':
				$last_transactions = $this->get_last_transactions_recent($limit);
				break;

			case 'year':
				$last_transactions = $this->get_last_transactions_year($limit);
				break;

			case 'month':
				$last_transactions = $this->get_last_transactions_month($limit);
				break;

			default:
				# code...
				break;
		}

		if(count($last_transactions) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($last_transactions); $i++){

				$operation = $last_transactions[$i];
				$operation['time'] = DateTime::createFromFormat('Y-m-d', $operation['time']);
				$operation['category'] = parent::get_category($operation['category']);
				$operation['payment'] = parent::get_payment($operation['payment']);

				include("view/private/operation.php");
			}
		}
	}

	public function print_last_transactions_year_current($limit){
		$last_transactions = $this->get_last_transactions_this_year($limit);

		if(count($last_transactions) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($last_transactions); $i++){

				$operation = $last_transactions[$i];
				$operation['time'] = DateTime::createFromFormat('Y-m-d', $operation['time']);
				$operation['category'] = parent::get_category($operation['category']);
				$operation['payment'] = parent::get_payment($operation['payment']);

				include("view/private/operation.php");
			}
		}
	}


	public function print_last_transactions_month_current($limit){
		$last_transactions = $this->get_last_transactions_this_month($limit);

		if(count($last_transactions) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($last_transactions); $i++){

				$operation = $last_transactions[$i];
				$operation['time'] = DateTime::createFromFormat('Y-m-d', $operation['time']);
				$operation['category'] = parent::get_category($operation['category']);
				$operation['payment'] = parent::get_payment($operation['payment']);

				include("view/private/operation.php");
			}
		}
	}


	public function get_last_transactions_added($limit){
		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id ORDER BY id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();
	}


	public function get_last_transactions_recent($limit){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id ORDER BY time DESC, id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();

	}


	public function get_last_transactions_this_year($limit){
		$current_year = date('Y');
		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year ORDER BY time DESC, id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':year', $current_year);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();

	}


	public function get_last_transactions_this_month($limit){
		$current_year = date('Y');
		$current_month = date('m');
		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year AND MONTH(time) = :month ORDER BY time DESC, id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':year', $current_year);
		$req->bindParam(':month', $current_month);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();

	}


	public function get_last_transactions_year($limit){
		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year ORDER BY time DESC, id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':year', $this->year);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();

	}


	public function get_last_transactions_month($limit){
		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year AND MONTH(time) = :month ORDER BY time DESC, id DESC LIMIT :limit');
		$req->bindParam(':id', $_SESSION['id']);
		$req->bindParam(':year', $this->year);
		$req->bindParam(':month', $this->month);
		$req->bindParam(':limit', $limit, PDO::PARAM_INT);
		$req->execute();
		return $req->fetchAll();

	}


	public function display_year_list(){

		$this->years = $this->get_all_years();
		for ($i=0; $i < count($this->years); $i++) {
			include("view/private/change_year.php");
		}

	}


	public function display_month_list(){

		$this->months = $this->get_all_months();
		for ($i=0; $i < count($this->months); $i++) {
			if($this->months[$i]['month'] < 10)
				$this->months[$i]['month'] = "0". $this->months[$i]['month'];
			include("view/private/change_month.php");
		}

	}


	protected function get_all_years(){

		$req = self::$db->prepare('SELECT DISTINCT YEAR(time) as year FROM '. dbName .'.operation WHERE user = :id ORDER BY year');
		$req->execute([':id' => $_SESSION['id']]);
		return $req->fetchAll();

	}


	protected function get_all_months(){

		$req = self::$db->prepare('SELECT DISTINCT YEAR(time) as year, MONTH(time) as month FROM '. dbName .'.operation WHERE user = :id ORDER BY year, month');
		$req->execute([':id' => $_SESSION['id']]);
		return $req->fetchAll();

	}


	public function get_correct_month_name($month = null){
		if(! $month)
			$month = $this->month;
		$month_txt = ucfirst(strftime("%B", mktime(20, 0, 0, $month, 1, $this->year)));

		if($month == 4 || $month == 8 || $month == 10)
			return "d'". $month_txt;
		else
			return "de ". $month_txt;
	}


	public function get_account_value($type){

		$current_year = date('Y');
		$current_month = date('m');

		switch ($type) {
			case 'all':
				$req = self::$db->prepare('SELECT TRUNCATE(SUM(amount), 2) as total FROM '. dbName .'.operation WHERE user = :id');
				$req->execute([':id' => $_SESSION['id']]);
				break;

			case 'year':
				$req = self::$db->prepare('SELECT TRUNCATE(SUM(amount), 2) as total FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year
				]);
				break;

			case 'month':

				$req = self::$db->prepare('SELECT TRUNCATE(SUM(amount), 2) as total FROM '. dbName .'.operation WHERE user = :id AND YEAR(time) = :year AND MONTH(time) = :month');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year,
					':month' => $current_month
				]);
				break;

			default:
				exit(1);
				break;
		}
		return $req->fetch()['total'];
	}


	public function get_current_correct_month_name(){
		return $this->get_correct_month_name(date('m'));
	}


	public function print_categories($type){

		$categories = $this->get_sum_categories($type);

		if(count($categories) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($categories); $i++){
				$category = $categories[$i];
				include("view/private/category.php");
			}
		}
	}


	public function get_sum_categories($type){

		$current_year = date('Y');
		$current_month = date('m');

		switch ($type) {
			case 'all':

				$req = self::$db->prepare('SELECT category.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.category on operation.category = category.id WHERE operation.user = :id GROUP BY operation.category ORDER BY total DESC');
				$req->execute([':id' => $_SESSION['id']]);
				break;

			case 'year':
				$req = self::$db->prepare('SELECT category.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.category on operation.category = category.id WHERE operation.user = :id AND YEAR(operation.time) = :year GROUP BY operation.category ORDER BY total DESC');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year
				]);
				break;

			case 'month':

				$req = self::$db->prepare('SELECT category.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.category on operation.category = category.id WHERE operation.user = :id AND YEAR(operation.time) = :year AND MONTH(operation.time) = :month GROUP BY operation.category ORDER BY total DESC');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year,
					':month' => $current_month
				]);
				break;

			default:
				exit(1);
				break;
		}

		return $req->fetchAll();
	}


	public function print_payments($type){

		$payments = $this->get_sum_payments($type);

		if(count($payments) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($payments); $i++){
				$payment = $payments[$i];
				include("view/private/payment.php");
			}
		}

	}


	public function print_total_selected_month(){

		$req = self::$db->prepare('SELECT TRUNCATE(SUM(amount), 2) as total FROM '. dbName .'.operation WHERE operation.user = :id AND YEAR(operation.time) = :year AND MONTH(operation.time) = :month');
		$req->execute([
			':id' => $_SESSION['id'],
			':year' => $this->year,
			':month' => $this->month
		]);

		$amount = $req->fetch()['total'];

		if($amount > 0)
			print('<span class="positiveamount">'. $amount .'</span>');
		else
			print('<span class="negativeamount">'. $amount .'</span>');
	}


	public function get_total_selected_month(){

		$req = self::$db->prepare('SELECT TRUNCATE(SUM(amount), 2) as total FROM '. dbName .'.operation WHERE operation.user = :id AND YEAR(operation.time) = :year AND MONTH(operation.time) = :month');
		$req->execute([
			':id' => $_SESSION['id'],
			':year' => $this->year,
			':month' => $this->month
		]);

		$amount = $req->fetch()['total'];

		return $amount;
	}


	public function get_sum_payments($type){

		$current_year = date('Y');
		$current_month = date('m');

		switch ($type) {
			case 'all':

				$req = self::$db->prepare('SELECT payment.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.payment on operation.payment = payment.id WHERE operation.user = :id GROUP BY operation.payment ORDER BY total DESC');
				$req->execute([':id' => $_SESSION['id']]);
				break;

			case 'year':
				$req = self::$db->prepare('SELECT payment.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.payment on operation.payment = payment.id WHERE operation.user = :id AND YEAR(operation.time) = :year GROUP BY operation.payment ORDER BY total DESC');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year
				]);
				break;

			case 'month':

				$req = self::$db->prepare('SELECT payment.name, TRUNCATE(SUM(amount), 2) as total, COUNT(operation.id) as nb FROM '. dbName .'.operation LEFT JOIN '. dbName .'.payment on operation.payment = payment.id WHERE operation.user = :id AND YEAR(operation.time) = :year AND MONTH(operation.time) = :month GROUP BY operation.payment ORDER BY total DESC');
				$req->execute([
					':id' => $_SESSION['id'],
					':year' => $current_year,
					':month' => $current_month
				]);
				break;

			default:
				exit(1);
				break;
		}

		return $req->fetchAll();
	}


	public function print_recap(){

		print("<h2>Récapitulatifs</h2>");


		$amount = $this->get_account_value("month");
		if($amount){

			if($amount > 0)
				$amount = '<span class="positiveamount">'. $amount .'</span>';
			else
				$amount = '<span class="negativeamount">'. $amount .'</span>';

			print('<p>
				Somme des opérations ('. $this->get_current_correct_month_name() .') : <strong><em>'. $amount .'</em>  €</strong>
			</p>');
		}
		else
			include("view/private/no_operation_month.php");


		$year = date('Y');
		$amount = $this->get_account_value("year");
		if($amount){

			if($amount > 0)
				$amount = '<span class="positiveamount">'. $amount .'</span>';
			else
				$amount = '<span class="negativeamount">'. $amount .'</span>';

			print('<p>
				Somme des opérations (de '. $year .') : <strong><em>'. $amount .'</em>  €</strong>
			</p>');
		}
		else
			include("view/private/no_operation_year.php");


		$amount = $this->get_account_value("all");
		if($amount){

			if($amount > 0)
				$amount = '<span class="positiveamount">'. $amount .'</span>';
			else
				$amount = '<span class="negativeamount">'. $amount .'</span>';

			print('<p>
				Somme des opérations (total) : <strong><em>'. $amount .'</em> €</strong>
			</p>');
		}
		else
			include("view/private/no_operation.php");

	}

}
