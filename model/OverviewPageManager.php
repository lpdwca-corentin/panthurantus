<?php

class OverviewPageManager extends Base
{
	private $title;
	private $description;
	private $h1;


	function __construct($title, $description, $h1)
	{
		parent::__construct();
		$this->h1 = htmlspecialchars($h1);
		$this->title = htmlspecialchars($title);
		$this->description = htmlspecialchars($description);
		$this->check_login();

		require_once("model/BoardsPageManager.php");
		$this->boards = new BoardsPageManager("", "", "");
	}


	public function name(){
		return 0;
	}


	public function render(){
		include("layout/head.php");
		include("view/public/menu.php");
		include("view/public/overview.php");
		include("layout/footer.php");
	}


	public function get_title(){
		return $this->title;
	}


	public function get_description(){
		return $this->description;
	}


	public function get_h1(){
		return $this->h1;
	}


	protected function check_login(){
		parent::check_login();
	}


	public function get_first_name(){
		return $_SESSION['first_name'];
	}


	public function print_last_transactions(){

		$last_transactions = $this->get_last_transactions_added();

		if(count($last_transactions) == 0){
			include("view/private/no_operation.php");
		}
		else{
			for($i = 0; $i < count($last_transactions); $i++){

				$operation = $last_transactions[$i];
				$operation['time'] = DateTime::createFromFormat('Y-m-d', $operation['time']);
				$operation['category'] = parent::get_category($operation['category']);
				$operation['payment'] = parent::get_payment($operation['payment']);

				include("view/private/operation.php");
			}
		}

	}


	public function get_last_transactions(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id ORDER BY time DESC, id DESC LIMIT 10');
		$req->execute([':id' => $_SESSION['id']]);
		return $req->fetchAll();

	}


	public function get_last_transactions_added(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE user = :id ORDER BY id DESC LIMIT 10');
		$req->execute([':id' => $_SESSION['id']]);
		return $req->fetchAll();

	}


	private function get_user($id){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE id = :id');
		$req->execute([':id' => $id]);
		return $req->fetch();

	}



	public function get_current_correct_month_name(){
		$this->boards->get_current_correct_month_name();
	}


	public function print_account_value($type){
		$this->boards->print_account_value($type);
	}

	public function print_recap(){
		$this->boards->print_recap();
	}

}
