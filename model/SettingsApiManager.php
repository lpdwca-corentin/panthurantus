<?php
class SettingsApiManager extends Base
{

	function __construct()
	{
		parent::__construct();
		$this->password = $_POST['password'];
		$this->newpassword = $_POST['newpassword'];
		$this->newemail = htmlspecialchars($_POST['email']);
		$this->newfirst_name = htmlspecialchars($_POST['first_name']);
		$this->newlast_name = htmlspecialchars($_POST['last_name']);
	}


	public function update_settings(){

		$this->check_email_exist();

		$this->check_password_match();
	}

	private function check_email_exist(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE email = :email');
		$req->execute([':email' => $_SESSION['email']]);
		$account = $req->fetch();

		if(isset($account['password']) && $_SESSION['id'] != $account['id']){

			$_SESSION['notif'] = ['error', "Cette adresse email appartient déjà à un compte."];
			header('Location: /settings/');

		}

	}

	private function check_password_match(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE email = :email');
		$req->execute([':email' => $_SESSION['email']]);
		$account = $req->fetch();

		if(password_verify($this->password, $account['password'])){

			if($this->newpassword != "")
				$pass = $this->newpassword;
			else
				$pass = $this->password;

			$req = self::$db->prepare('UPDATE '. dbName .'.user SET email = :email, password = :password, first_name = :first_name, last_name = :last_name WHERE id = :id');
			$req->execute([
				'email' => $this->newemail,
				'password' => password_hash($pass, PASSWORD_BCRYPT),
				'first_name' => $this->newfirst_name,
				'last_name' => $this->newlast_name,
				'id' => $_SESSION['id']
			]);

			$_SESSION['email'] = $this->newemail;
			$_SESSION['first_name'] = $this->newfirst_name;
			$_SESSION['last_name'] = $this->newlast_name;

			# get new hash of password
			$req = self::$db->prepare('SELECT password FROM '. dbName .'.user WHERE id = :id');
			$req->execute([':id' => $_SESSION['id']]);
			$new_hashed_password = $req->fetch()['password'];

			# create new session id with new hash of password so Base::check_login will not find any problem
			$_SESSION['sid'] = password_hash($account['id'] . $new_hashed_password, PASSWORD_BCRYPT);

			$_SESSION['notif'] = ['success', "Les paramètres ont bien été modifiés."];
			header('Location: /overview/');
		}
		else{
			$_SESSION['notif'] = ['error', "Votre mot de passe ne correspond pas."];
			header('Location: /settings/');
		}

	}

}
