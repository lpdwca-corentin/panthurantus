<?php

class changeYearApiManager extends Base
{

	function __construct()
	{
		parent::__construct();
	}

	public function updateYear($year){
		if(strlen($year) != 4){
			$_SESSION['msg'] = ['error', "L'année sélectionnée n'existe pas."];
			header('Location: /overview/');
		}
		else{
			$_SESSION['year'] = (int)$year;
			header('Location: /boards/year/');
		}
		return 0;
	}

}
