<?php
class LoginApiManager extends Base
{

	private $email;
	private $password;
	private $stored_password;


	public function render(){
		include("layout/head.php");
		include("view/public/base.php");
		include("layout/footer.php");
	}

	function __construct()
	{
		parent::__construct();
	}

	public function sanitize($email, $password){
		$this->email = htmlspecialchars($email);
		$this->password = $password;
	}

	public function login(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE email = :email');
		$req->execute([':email' => $this->email]);
		$account = $req->fetch();

		if(isset($account['password'])){
			if(password_verify($this->password, $account['password'])){
				$_SESSION['id'] = $account['id'];
				$_SESSION['email'] = $this->email;
				$_SESSION['first_name'] = $account['first_name'];
				$_SESSION['last_name'] = $account['last_name'];
				$_SESSION['sid'] = password_hash($account['id'] . $account["password"],  PASSWORD_BCRYPT);

				$_SESSION['notif'] = ['success', "Vous êtes maintenant connecté."];
				header('Location: /overview/');
				exit(1);
			}
			else{
				$_SESSION['notif'] = ['error', "Votre mot de passe ou votre identifiant est incorrect."];
				header('Location: /login/');
				exit(1);
			}
		}
		else
			$_SESSION['notif'] = ['error', "Votre mot de passe ou votre identifiant est incorrect."];
			header('Location: /login/');
			exit(1);
	}

}
