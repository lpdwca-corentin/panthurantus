<?php
class RegisterApiManager extends Base
{

	private $email;
	private $password;
	private $stored_password;


	public function render(){
		include("layout/head.php");
		include("view/public/base.php");
		include("layout/footer.php");
	}

	function __construct()
	{
		parent::__construct();
	}

	public function sanitize($code, $email, $password, $firstname, $lastname){
		$this->code = htmlspecialchars($code);
		$this->email = htmlspecialchars($email);
		$this->password = $password;
		$this->firstname = htmlspecialchars($firstname);
		$this->lastname = htmlspecialchars($lastname);
	}


	public function register(){

		if($this->test_account_already_exist($this->email)){
			$_SESSION['notif'] = ['error', "Un compte existe déjà avec cette adresse email."];
			header('Location: /register/');
			exit(1);
		}

		if(! $this->test_code_is_available($this->code)){
			$_SESSION['notif'] = ['error', "Ce code n'est pas valide."];
			header('Location: /register/');
			exit(1);
		}

		$this->save_account();
		$_SESSION['notif'] = ['success', "Votre compte a été créé, vous pouvez vous connecter !"];
		header('Location: /login/');

	}


	private function test_account_already_exist($email){

		$req = self::$db->prepare('SELECT email FROM '. dbName .'.user WHERE email = :email');
		$req->execute([':email' => $email]);
		$account = $req->fetch();

		if(isset($account['email']))
			return true;
		else // register user
			return false;

	}


	private function test_code_is_available($code){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE activation = :code');
		$req->execute([':code' => $code]);
		$account = $req->fetch();

		// only activation code ? this account will be created
		if($account['activation'] == $code && empty($account['email']) && empty($account['first_name']) && empty($account['last_name']))
			return true;
		else // code isnt available or doesn't exist
			return false;

	}

	private function save_account(){
		$req = self::$db->prepare('UPDATE '. dbName .'.user SET email = :email, password = :password, first_name = :first_name, last_name = :last_name WHERE activation = :activation');
		$req->execute([
			'email' => $this->email,
			'password' => password_hash($this->password, PASSWORD_BCRYPT),
			'first_name' => $this->firstname,
			'last_name' => $this->lastname,
			'activation' => $this->code
		]);
	}

}
