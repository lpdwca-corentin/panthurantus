<?php
class OperationApiManager extends Base
{

	function __construct()
	{
		parent::__construct();
	}

	private function sanitize($time, $amount, $payment, $category){
		if(! (
			(float)$amount == $amount &&
			ctype_digit($payment) &&
			ctype_digit($category) &&
			DateTime::createFromFormat('Y-m-d', $time)
		)){

			$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la lecture des champs."];
			header('Location: /'. $parent::current_page .'/');
			exit(1);
		}

		$this->time = $time;
		$this->amount = (float)$amount;
		$this->payment = (int)$payment;
		$this->category = (int)$category;
	}


	public function createCredit($time, $amount, $payment, $category){
		$this->sanitize($time, $amount, $payment, $category);

		$req = self::$db->prepare('INSERT INTO '. dbName .'.operation (user, time, amount, category, payment) VALUES (:user, :time, :amount, :category, :payment)');
		$req->execute([
			':user' => $_SESSION['id'],
			':time' => $this->time,
			':amount' => $this->amount,
			':category' => $this->category,
			':payment' => $this->payment
		]);

		if( abs($this->amount) != $this->amount){
			$_SESSION['notif'] = ['success', "Votre débit a bien été enregistré."];
			if(isset($_SESSION['current_page']))
				header('Location: /'. $_SESSION['current_page'] .'/');
			else
				header('Location: /overview/');
		}
		else{
			$_SESSION['notif'] = ['success', "Votre crédit a bien été enregistré."];
			if(isset($_SESSION['current_page']))
				header('Location: /'. $_SESSION['current_page'] .'/');
			else
				header('Location: /overview/');
		}
	}


	public function createDebit($time, $amount, $payment, $category){
		$amount = - abs( (float) $amount);
		$this->sanitize($time, $amount, $payment, $category);

		$this->createCredit($time, $amount, $payment, $category);
	}

}
