<?php
class Base
{

	protected static $db;

	function __construct()
	{
		$this->dbConnect();
  		$this->today = date("Y-m-d");
  		$this->payments = $this->get_payments();
  		$this->categories = $this->get_categories();
		if(isset($_SESSION['notif'])){
			$this->msg = $_SESSION['notif'];
			unset($_SESSION['notif']);
		}
		if(isset($_GET['page'])){
			if(isset($_GET['action']))
				$_SESSION['current_page'] = $_GET['page'] ."/". $_GET['action'];
			else
				$_SESSION['current_page'] = $_GET['page'];
		}
	}

	protected function dbConnect()
	{
		if (!(self::$db instanceof PDO))
			self::$db = new PDO('mysql:host='. dbAddr .';dbname='. dbName .';charset=utf8mb4', dbUser, dbPass);
	}

	protected function check_login(){
		if(!isset($_SESSION['sid']) || !isset($_SESSION['id'])){
			$_SESSION['notif'] = ['error', "Vous avez été déconnecté à cause de votre inactivité. Veuillez vous reconnecter."];
			header('Location: /login/');
			exit(1);
		}

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.user WHERE id = :id');
		$req->execute([':id' => $_SESSION['id']]);
		$account = $req->fetch();

		if(!isset($account['password'])){

			$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la vérification de votre authentification. Veuillez vous reconnecter."];
			header('Location: /login/');
			exit(1);
		}

		if(!password_verify($_SESSION['id'] . $account["password"], $_SESSION['sid'])){

			$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la vérification de votre authentification. Veuillez vous reconnecter."];
			header('Location: /login/');
			exit(1);
		}

		$_SESSION['email'] = $account['email'];
		$_SESSION['first_name'] = $account['first_name'];
		$_SESSION['last_name'] = $account['last_name'];
		$_SESSION['id'] = $account['id'];
	}

	public function render(){
		include("layout/head.php");
		include("view/public/base.php");
		include("layout/footer.php");
	}


	protected function get_category($category){

		$req = self::$db->prepare('SELECT name FROM '. dbName .'.category WHERE id = :category');
		$req->execute([':category' => $category]);
		return $req->fetch()['name'];

	}


	protected function get_payment($payment){

		$req = self::$db->prepare('SELECT name FROM '. dbName .'.payment WHERE id = :payment');
		$req->execute([':payment' => $payment]);
		return $req->fetch()['name'];

	}


	private function get_payments(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.payment ORDER BY id');
		$req->execute();
		return $req->fetchAll();

	}


	private function get_categories(){

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.category ORDER BY id');
		$req->execute();
		return $req->fetchAll();

	}


	public function pluralize($amount){

		if($amount > 1)
			print("s");

	}

}
