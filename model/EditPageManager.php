<?php

class EditPageManager extends Base
{
	private $title;
	private $description;
	private $h1;


	function __construct($title, $description, $h1)
	{
		parent::__construct();
		$this->check_login();
		$this->title = htmlspecialchars($title);
		$this->description = htmlspecialchars($description);
		$this->h1 = htmlspecialchars($h1);
	}


	public function render(){
		include("layout/head.php");
		include("view/public/menu.php");
		include("view/private/edit_operation.php");
		include("layout/footer.php");
	}

	private function sanitize($time, $amount, $type, $payment, $category){
		if(! (
			(float)$amount == $amount &&
			ctype_digit($payment) &&
			ctype_digit($category) &&
			DateTime::createFromFormat('Y-m-d', $time)
		)){

			$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la lecture des champs."];
			header('Location: /'. $parent::current_page .'/');
			exit(1);
		}

		$this->time = $time;
		if($type == "on")
			$this->amount = (float)$amount;
		else
			$this->amount = - abs( (float) $amount);
		$this->payment = (int)$payment;
		$this->category = (int)$category;
	}

	public function get_title(){
		return $this->title;
	}


	public function get_description(){
		return $this->description;
	}


	public function get_h1(){
		return $this->h1;
	}


	public function edit($id){

		$this->id = $id;

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE id = :id AND user = :user');
		$req->execute([
			":id" => $id,
			":user" => $_SESSION['id']
		]);
		$this->operation =  $req->fetch();

		if(!$this->operation){
			$_SESSION['notif'] = ['error', "Vous ne pouvez pas éditer cette opération."];
			header('Location: /overview/');
			exit(1);
		}

		$this->render();

	}

	public function submit($id, $type, $time, $amount, $payment, $category){
		$this->sanitize($time, $amount, $type, $payment, $category);

		$req = self::$db->prepare('UPDATE '. dbName .'.operation SET time = :time, amount = :amount, payment = :payment, category = :category WHERE id = :id');
		$req->execute([
			'time' => $this->time,
			'amount' => $this->amount,
			'payment' => $this->payment,
			'category' => $this->category,
			'id' => $id
		]);


		$_SESSION['notif'] = ['success', "L'opération a bien été modifiée."];
		header('Location: /overview/');
	}

	public function delete($id){

		$this->id = $id;

		$req = self::$db->prepare('SELECT * FROM '. dbName .'.operation WHERE id = :id AND user = :user');
		$req->execute([
			":id" => $id,
			":user" => $_SESSION['id']
		]);
		$this->operation =  $req->fetch();

		if(!$this->operation){
			$_SESSION['notif'] = ['error', "Vous ne pouvez pas supprimer cette opération."];
			header('Location: /overview/');
			exit(1);
		}

		$req = self::$db->prepare('DELETE FROM '. dbName .'.operation WHERE id = :id');
		$req->execute([
			":id" => $id
		]);

		$_SESSION['notif'] = ['success', "L'opération a été supprimée avec succèes."];
		header('Location: /overview/');



	}

}
