<?php

class ChangeMonthApiManager extends Base
{

	function __construct()
	{
		parent::__construct();
	}

	public function updateMonth($month, $year){
		if(strlen($year) != 4){
			$_SESSION['msg'] = ['error', "L'année sélectionnée n'existe pas."];
			header('Location: /overview/');
		}
		else{
			if(strlen($month) != 2){
				$_SESSION['msg'] = ['error', "Le mois sélectionné n'existe pas."];
			header('Location: /overview/');
			}
			else{
				$_SESSION['year'] = (int)$year;
				$_SESSION['month'] = (int)$month;
				header('Location: /boards/month/');
			}
		}
		return 0;
	}

}
