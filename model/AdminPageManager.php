<?php

class AdminPageManager extends Base
{
	private $title;
	private $description;
	private $h1;


	function __construct($title, $description, $h1)
	{
		parent::__construct();
		$this->check_login();
		if($_SESSION['id'] != 1){
			$_SESSION['notif'] = ['error', "Erreur inconnue."];
			header('Location: /');
			exit(1);
		}
		$this->title = htmlspecialchars($title);
		$this->description = htmlspecialchars($description);
		$this->h1 = htmlspecialchars($h1);
	}


	public function render(){
		$this->process_msg();
		include("layout/head.php");
		include("view/public/menu.php");
		include("view/private/admin.php");
		include("layout/footer.php");
	}


	public function get_title(){
		return $this->title;
	}


	public function get_description(){
		return $this->description;
	}


	public function get_h1(){
		return $this->h1;
	}


	protected function check_login(){
		parent::check_login();
	}


	private function get_accounts(){

		$req = self::$db->prepare('SELECT id, activation, email, first_name, last_name FROM '. dbName .'.user ORDER BY id');
		$req->execute();
		return $req->fetchAll();

	}

	public function list_accounts(){
		$accounts = $this->get_accounts();

		for($i = 0; $i < count($accounts); $i++){
			$account = $accounts[$i];
			include("view/private/list_accounts.php");
		}
	}

	public function process_msg(){
		if(isset($_GET['msg'])){
			$msg = htmlspecialchars($_GET['msg']);
			switch ($msg) {

				default:
					# no error/success, nothing to display
					break;
			}
		}
	}

	public function add_keys(){
		if(isset($_POST['addkeys'])){
			$keys = explode("\n", $_POST['addkeys']);

			$nb = 0;
			foreach ($keys as $index => $key){
				if($key != "" && $key != "\n" && strlen(trim($key)) > 0){
					$nb++;
					$this->add_key(htmlspecialchars(rtrim($key)));
				}
			}
		}

		$_SESSION['notif'] = ['success', "Les $nb clef ont bien été ajoutées."];
		header('Location: /admin/');
		exit(1);
	}


	private function add_key($key){
		$req = self::$db->prepare('INSERT INTO '. dbName .'.user (activation, email, password, first_name, last_name) VALUES (:activation, "", "", "", "")');
		$req->execute([':activation' => $key]);
	}

}
