	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>

	<section>

		<h2>Article 1 : Objet</h2>

		<p>
			Les présentes CGU ou Conditions Générales d’Utilisation encadrent juridiquement l’utilisation des services du site Panthurantus (ci-après dénommé "le site").
		</p>

		<p>
			Panthurantus représente le travail de Corentin Bettiol (ci-après dénommé "Dieu") sur la SP (Situation Problème) du cours de PHP (UE 6.1) de la LPDWCA (LICENCE PROFESSIONNELLE DÉVELOPPEMENT WEB, COMMUNICATION ET APPRENTISSAGES). Ce n'est donc qu'un site "fictif", en cela qu'il ne sera jamais utilisable par d'autres personnes que les personnes concernées par la LPDWCA.
		</p>

		<h2>Article 2 : Mentions légales</h2>

		<p>
			L’édition du site est assurée par Corentin Bettiol, résidant dans le sud de la France Métropolitaine.
		</p>

		<p>
			L’hébergeur du site est Alwaysdata.
		</p>

		<h2>Article 3 : Accès au site</h2>

		<p>
			Le site permet d’accéder gratuitement aux services suivants :
		</p>
		<ul>
			<li>Suivi de budget fictif,</li>
			<li>Visualisation de graphiques,</li>
			<li>Entrer des transactions.</li>
		</ul>

		<p>
			Le site est accessible gratuitement depuis n’importe où par tout utilisateur disposant d’un accès à Internet. Tous les frais nécessaires pour l’accès aux services (matériel informatique, connexion Internet…) sont à la charge de l’utilisateur.
		</p>

		<p>
			L’accès aux services dédiés aux membres s’effectue à l’aide d’un code d'accès lors de la première utilisation, puis d'un identifiant et d’un mot de passe.
		</p>

		<p>
			Pour des raisons de maintenance ou autres, l’accès au site peut être interrompu ou suspendu par l’éditeur sans préavis ni justification.
		</p>

		<h2>Article 4 : Collecte des données</h2>

		<p>
			Pour la création du compte de l’Utilisateur, la collecte des informations au moment de l’inscription sur le site est nécessaire et obligatoire. Conformément à la loi n°78-17 du 6 janvier relative à l’informatique, aux fichiers et aux libertés, la collecte et le traitement d’informations personnelles s’effectuent dans le respect de la vie privée.
		</p>

		<p>
			Suivant la loi Informatique et Libertés en date du 6 janvier 1978, articles 39 et 40, l’Utilisateur dispose du droit d’accéder, de rectifier, de supprimer et d’opposer ses données personnelles. L’exercice de ce droit s’effectue en envoyant un email à l'adresse suivante: "games", suivi d'un arobase et de "24446666", point, "xyz".
		</p>

		<h2>Article 5 : Propriété intellectuelle</h2>

		<p>
			Les marques, logos ainsi que les contenus du site lapetiteperle (illustrations graphiques, textes…) sont protégés par le Code de la propriété intellectuelle et par le droit d’auteur.
		</p>

		<p>
			La reproduction et la copie des contenus par l’Utilisateur requièrent une autorisation préalable du site. Dans ce cas, toute utilisation à des usages commerciaux ou à des fins publicitaires est proscrite.
		</p>

		<h2>Article 6 : Responsabilité</h2>

		<p>
			Bien que les informations publiées sur le site soient réputées fiables, le site se réserve la faculté d’une non-garantie de la fiabilité des sources.
		</p>

		<p>
			Les informations diffusées sur le site lapetiteperle sont présentées à titre purement informatif et sont sans valeur contractuelle. En dépit des mises à jour régulières, la responsabilité du site ne peut être engagée en cas de modification des dispositions administratives et juridiques apparaissant après la publication. Il en est de même pour l’utilisation et l’interprétation des informations communiquées sur la plateforme.
		</p>

		<h2>Article 7 : Liens hypertextes</h2>

		<p>
			Le site peut être constitué de liens hypertextes. En cliquant sur ces derniers, l’Utilisateur pourra sortir de la plateforme. Cette dernière n’a pas de contrôle et ne peut pas être tenue responsable du contenu des pages web relatives à ces liens.
		</p>

		<h2>Article 8 : Cookies</h2>

		<p>
			Lors des visites sur le site, l’installation automatique d’un cookie sur le logiciel de navigation de l’Utilisateur peut survenir.
		</p>

		<p>
			Les cookies correspondent à de petits fichiers déposés temporairement sur le disque dur de l’ordinateur de l’Utilisateur. Ces cookies sont nécessaires pour assurer l’accessibilité et la navigation sur le site. Ces fichiers ne comportent pas d’informations personnelles et ne peuvent pas être utilisés pour l’identification d’une personne.
		</p>

		<p>
			L’information présente dans les cookies est utilisée pour permettre à l'Utilisateur d'utiliser le service.
		</p>
	</section>

	<a class="button" href="/">Retour à l'accueil du site</a>
