	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>

	<section>
		<p>
			Connectez-vous pour profiter de toutes les fonctionnalités offertes par Panthurantus !
		</p>
		<p><a href="/">Retour à l'accueil</a>.</p>
	</section>

	<section>
		<form action="/api/login/" method="post">
			<label for="email">Email :</label>
			<input type="email" name="email" id="email" required />

			<label for="password">Mot de passe :</label>
			<input type="password" name="password" id="password" required />

			<input type="submit" value="Se connecter" />
		</form>
	</section>

	<section class="visible">
		<p>
			Vous n'avez pas de compte, mais vous avez reçu un code d'activation ?<br />
			<a href="/register/" class="visible">Venez finaliser votre inscription</a> !
		</p>
	</section>
