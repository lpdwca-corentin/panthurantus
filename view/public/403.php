	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>

	<section>
		<p>
			Ce contenu n'est pas disponible.
		</p>
	</section>

	<section>
		<p>
			L'erreur 403 est levée lorsque vous ne disposez pas des droits nécessaires pour afficher un contenu.
		</p>
	</section>

	<a class="button" href="/">Retour à l'accueil du site</a>
