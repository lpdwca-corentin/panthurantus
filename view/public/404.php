	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>

	<section>
		<p>
			Cette page n'existe pas !
		</p>
	</section>

	<section>
		<p>
			Plus précisement, vous avez cherché à accéder à une ressource via une URL mal construite, ou alors la page à laquelle vous tentez d'accéder n'existe plus.
		</p>
	</section>

	<a class="button" href="/">Retour à l'accueil du site</a>
