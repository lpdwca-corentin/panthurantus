	<h1><?php print($this->get_h1()); ?></h1>

	<section>
		<p>
			La page des paramètres est celle qui vous permet d'éditer les informations relatives à votre compte.
		</p>
	</section>


	<section>
		<form method="post" action="/api/settings/">
			<label for="first_name">Prénom :</label>
			<input type="text" name="first_name" id="first_name" value="<?php print($_SESSION['first_name']); ?>" />


			<label for="last_name">Nom :</label>
			<input type="text" name="last_name" id="last_name" value="<?php print($_SESSION['last_name']); ?>" />

			<label for="email">Email :</label>
			<input type="email" name="email" id="email" value="<?php print($_SESSION['email']); ?>" />

			<label for="password">Mot de passe actuel :</label>
			<input type="password" name="password" id="password" />

			<label for="newpassword">Nouveau mot de passe (<i>laisser vide pour garder le mot de passe actuel</i>) :</label>
			<input type="password" name="newpassword" id="newpassword" />

			<input type="submit" value="Envoyer" />
		</form>
	</section>
