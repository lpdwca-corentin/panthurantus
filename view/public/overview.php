	<h1><?php print($this->get_h1()); ?></h1>

	<?php include("view/private/add_operation.php"); ?>

	<section>
		<p>
			Bienvenue, <?php print($this->get_first_name()); ?>. Vous pouvez trouver sur cette page vos dernières opérations.
		</p>
	</section>


	<section>
		<?php $this->print_recap(); ?>
	</section>


	<section id="overviewBoard">
		<h2>Dernières opérations</h2>

		<p><a href="/boards/">Accéder aux tableaux détaillés</a>.</p>

		<?php $this->print_last_transactions(); ?>

	</section>


	<section id="overviewGraph">
		<h2>Graphiques</h2>

		<canvas id="stackedBars"></canvas>
	</section>

	<?php include("view/private/new_operation.php"); ?>
	<script src="/static/js/Chart.min.js"></script>
	<script src="/static/js/charts.js"></script>
