	<h1><?php print($this->get_h1()); ?></h1>

	<?php include("view/private/add_operation.php"); ?>

	<section>
		<p>Le but de cette page est de faire parler les chiffres, de montrer des tendances que vous n'auriez peut-être pas remarqué, ou bien de confirmer des impressions.</p>
	</section>


	<section>
		<canvas id="stackedBars"></canvas>
	</section>

	<section>
		<canvas id="barsCategories"></canvas>
	</section>

	<section>
		<canvas id="barsPayments"></canvas>
	</section>

	<section>
		<canvas id="linesCategories"></canvas>
	</section>



	<?php include("view/private/new_operation.php"); ?>
	<script src="/static/js/Chart.min.js"></script>
	<script src="/static/js/charts.js"></script>
