	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>

	<section>
		<p>
			C'est sur cette page que se passe l'inscription à l'application web.
		</p>
		<p>
			Vous avez déjà un compte ? <a href="/login/">Connectez-vous</a> !
		</p>
		<p><a href="/">Retour à l'accueil</a>.</p>
	</section>

	<section>
		<form action="/api/register/" method="post">
			<label for="code">Code d'activation :</label>
			<input type="text" name="code" id="code" />

			<label for="email">Email :</label>
			<input type="email" name="email" id="email" />

			<label for="password">Mot de passe :</label>
			<input type="password" name="password" id="password" />

			<span class="separator"></span>

			<label for="firstname">Prénom :</label>
			<input type="text" name="firstname" id="firstname" />

			<label for="lastname">Nom :</label>
			<input type="text" name="lastname" id="lastname" />

			<input type="submit" value="S'inscrire" />
		</form>
	</section>

	<section>
		<p>Voici quelques indications qui peuvent vous être utiles :</p>
		<p class="small pleft">
			Choisissez un mot de passe unique et plutôt long (vous pouvez utiliser un gestionnaire de mot de passe pour ne pas avoir à le retenir).
		</p>
		<p class="small pleft">
			Nous ne vous enverrons jamais d'email, et nous ne comptons pas vendre votre adresse.
		</p>
	</section>
