	<h1><?php print($this->get_h1()); ?></h1>

	<?php include("view/private/add_operation.php"); ?>

	<section>
		<p>C'est sur cette page que vous allez pouvoir trouver vos tableaux détaillés.</p>
	</section>

	<section>

		<h2>Historique</h2>

		<h3>Mois <?php print($this->get_current_correct_month_name()); ?></h3>
		<p><a href="/boards/month/">Voir toutes les opérations de tous les mois</a>.</p>

		<?php $this->print_last_transactions_month_current(5); ?>


		<h3>Opérations de <?php print(date('Y')); ?></h3>
		<p><a href="/boards/year/">Voir toutes les opérations de toutes les années</a>.</p>

		<?php $this->print_last_transactions_year_current(5); ?>

		<h3>Toutes les opérations</h3>
		<p><a href="/boards/recent/">Voir toutes les opérations, triées de la plus récente à la plus ancienne</a>.</p>
		<p><a href="/boards/added/">Voir toutes les opérations, les dernières ajoutées en premier</a>.</p>

	</section>

	<section>

		<h2>Catégories</h2>

		<h3>Mois <?php print($this->get_current_correct_month_name()); ?></h3>
		<?php $this->print_categories("month"); ?>


		<h3>Opérations de <?php print(date('Y')); ?></h3>
		<?php $this->print_categories("year"); ?>

		<h3>Total</h3>
		<?php $this->print_categories("all"); ?>

	</section>

	<section>

		<h2>Moyens de paiement</h2>

		<h3>Mois <?php print($this->get_current_correct_month_name()); ?></h3>
		<?php $this->print_payments("month"); ?>

		<h3>Opérations de <?php print(date('Y')); ?></h3>
		<?php $this->print_payments("year"); ?>

		<h3>Total</h3>
		<?php $this->print_payments("all"); ?>

	</section>

	<?php include("view/private/new_operation.php"); ?>
