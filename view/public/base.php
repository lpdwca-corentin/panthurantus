	<h1 class="homepage"><?php print($this->get_h1()); ?></h1>


	<a class="button smallbutton" href="/login/">Se connecter à l'application</a>

	<section>

		<h2>L'<b>application</b> web qui fait parler vos <b>transactions</b></h2>


		<aside>
			<img src="/static/img/undraw_wallet.svg" alt="wallet" />
		</aside>

		<p>
			Avec Panthurantus, vous pouvez suivre précisément vos dépenses et rentrées d'argent sur votre compte.
		</p>

		<aside>
			<img src="/static/img/undraw_finance.svg" alt="finances" />
		</aside>

		<p>
			Ses nombreuses features digitales vous permettent de mieux <b>visualiser</b> l'évolution de votre trésorerie.
		</p>

		<aside>
			<img src="/static/img/undraw_online_calendar.svg" alt="calendar" />
		</aside>

	</section>

	<section>

		<h2>Quelques exemples</h2>

		<p>
			Les <b>tableaux détaillés</b> vous permettent de trier votre budget par <b>mois</b>, <b>année</b>, <b>moyen de paiement</b> et <b>type de transaction</b>.
		</p>


		<aside>
			<img src="/static/img/detailed_tables.png" alt="graph" />
		</aside>

		<p>
			De nombreux <b>graphiques</b> vous permettent de mieux comprendre vos flux financiers.
		</p>

		<aside>
			<img src="/static/img/graph_evolution.png" alt="graph" />
		</aside>

		<p>
			C'est <b>vous</b> qui contrôlez le <b>contenu</b> de ces graphiques afin de dégager des tendances, mais vous pouvez <b>choisir</b> ce qu'ils doivent afficher.
		</p>

		<aside>
			<img src="/static/img/graph_1.gif" alt="display/hide elements on graph" />
		</aside>

	</section>

	<section>
		<h2>Une application qui respecte votre <b>vie privée</b></h2>

		<aside>
			<img src="/static/img/undraw_privacy_protection.svg" alt="privacy" />
		</aside>
		<p>
			Panturanthus est <b>stable</b>, <b><a target="_blank" href="https://gitlab.com/sodimel/panthurantus">open source</a></b>, dispose de nombreuses <b>fonctionnalités</b>, ne vous traque pas, et stocke vos données de manière <b>sécurisée</b>.
		</p>
	</section>

	<section>
		<h2>Un développement en plusieurs étapes</h2>
		<p>
			L'application n'est pas encore ouverte au public. Si vous disposez d'un code d'invitation, vous pouvez créer votre profil en cliquant sur le lien suivant :
		</p>
	</section>

	<a class="button" href="/login/">Accéder à Panthurantus</a>
