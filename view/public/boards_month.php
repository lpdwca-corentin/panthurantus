	<h1><?php print($this->get_h1()); ?></h1>

	<?php include("view/private/add_operation.php"); ?>

	<section>
		<p><a href="/boards/">Retour à la liste des tableaux détaillés</a>.</p>
		<p>Afficher un autre mois : <?php $this->display_month_list(); ?></p>
	</section>


	<section>
		<h2>Mois <?php print($this->get_correct_month_name()); ?> <?php print($this->year); ?></h2>


		<?php if($this->get_total_selected_month()){ ?>
			<p>
				Somme des opérations : <strong><?php $this->print_total_selected_month(); ?></strong> €
			</p>
		<?php } ?>

		<?php $this->print_last_transactions("month", 25000); ?>

	</section>

	<section>
		<p><a href="/boards/">Retour à la liste des tableaux détaillés</a>.</p>
	</section>

	<?php include("view/private/new_operation.php"); ?>
