	<nav id="mainMenu">
		<ul>
			<li <?php if($this->title == "Vue d'ensemble"){ ?>class="currentTab"<?php } ?>><a href="/overview/">Vue d'ensemble</a></li>
			<li <?php if($this->title == "Tableaux détaillés"){ ?>class="currentTab"<?php } ?>><a href="/boards/">Tableaux détaillés</a></li>
			<li <?php if($this->title == "Graphiques"){ ?>class="currentTab"<?php } ?>><a href="/graphs/">Graphiques</a></li>
			<li <?php if($this->title == "Paramètres"){ ?>class="currentTab"<?php } ?>><a href="/settings/" >Paramètres</a></li>
			<li><a href="/api/logout/">Déconnexion</a></li>
		</ul>
	</nav>
