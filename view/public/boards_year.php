	<h1><?php print($this->get_h1()); ?></h1>

	<?php include("view/private/add_operation.php"); ?>

	<section>
		<p><a href="/boards/">Retour à la liste des tableaux détaillés</a>.</p>
		<p>Afficher une autre année : <?php $this->display_year_list(); ?></p>
	</section>


	<section>
		<h2>Année <?php print($this->year); ?></h2>

		<?php $this->print_last_transactions("year", 25000); ?>

	</section>

	<section>
		<p><a href="/boards/">Retour à la liste des tableaux détaillés</a>.</p>
	</section>

	<?php include("view/private/new_operation.php"); ?>
