<div class="operation<?php if($payment['total'] > 0) print(" positive"); else print(" negative"); ?>">
	<div class="time">
		<?php print($payment['name']); ?>
	</div>
	<div class="category<?php if($payment['total'] > 0) print(" positiveamount"); else print(" negativeamount"); ?>">
		<?php print($payment['total']); ?> €
	</div>
	<div class="time">
		<em><?php print($payment['nb']); ?> opération<?php $this->pluralize($payment['nb']); ?></em>
	</div>
</div>
