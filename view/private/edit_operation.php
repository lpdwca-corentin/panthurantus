	<section>
		<h2>Édition d'opération</h2>

		<p>Vous souhaitez supprimer cette opération ? Cliquez ici : <a href="/api/delete/<?php echo $this->id; ?>/">supprimer l'opération</a>.</p>

		<div class="content">

			<form method="post" action="/api/edit/submit/<?php echo $this->id; ?>/">

				<label for="type" class="toggle">
					<span>Débit</span>
					<span class="switch">
						<input type="checkbox" name="type" id="type" <?php if($this->operation['amount'] > 0){
							echo "checked";
						} ?> />
						<span class="slider"></span>
					</span>
					<span>Crédit</span>
				</label>

				<label for="time">Date :</label>

				<input type="date" id="time" name="time" value="<?php echo($this->operation['time']); ?>" min="2000-01-01" required />

				<label for="amount">Montant :</label>

				<input type="number" step="0.01" min="0.01" name="amount" id="amount" value="<?php echo(abs($this->operation['amount'])); ?>" required />

				<label for="payment">Moyen de paiement :</label>

				<select name="payment" id="payment">
					<?php include("view/private/select_payment.php") ?>
				</select>

				<label for="category">Catégorie :</label>

				<select name="category" id="category">
					<?php include("view/private/select_category.php") ?>
				</select>


				<input type="submit" value="Valider les changements" />
			</form>
		</div>
	</section>
