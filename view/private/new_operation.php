	<section id="newOperation" class="overlay">
		<div class="popup">
			<a class="close" href="#">&times;</a>

			<h2>Nouvelle opération</h2>
			<p>Enregistrer une nouvelle opération.</p>

			<div class="content">

				<form method="post" action="/api/operation/">

					<label for="type" class="toggle">
						<span>Débit</span>
						<span class="switch">
							<input type="checkbox" name="type" id="type" />
							<span class="slider"></span>
						</span>
						<span>Crédit</span>
					</label>

					<label for="time">Date :</label>

					<input type="date" id="time" name="time" value="<?php echo($this->today); ?>" min="2000-01-01" required />

					<label for="amount">Montant :</label>

					<input type="number" step="0.01" min="0.01" name="amount" id="amount" required />

					<label for="payment">Moyen de paiement :</label>

					<select name="payment" id="payment">
						<?php include("view/private/select_payment.php") ?>
					</select>

					<label for="category">Catégorie :</label>

					<select name="category" id="category">
						<?php include("view/private/select_category.php") ?>
					</select>


					<input type="submit" value="Enregistrer" />
				</form>
			</div>
		</div>
	</section>
