<div class="operation<?php if($operation['amount'] > 0) print(" positive"); else print(" negative"); ?>">
	<div class="time">
		<?php print($operation['time']->format('d/m/Y')); ?>
	</div>
	<div class="amount">
		<?php print($operation['amount']); ?> €
	</div>
	<div class="category">
		<?php print($operation['category']); ?>
	</div>
	<div class="payment">
		<?php print($operation['payment']); ?>
	</div>
	<div class="edit">
		<a href="/api/edit/<?php print($operation['id']); ?>/">edit</a>
	</div>
</div>
