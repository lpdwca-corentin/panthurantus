
# Panthurantus
### SP Budget - UE6.1 - php & bdd - LPDWCA

![separateur](https://up.l3m.in/file/1586716705.png)

## Panthurantus est un site internet permettant de gérer son budget.

L'application permet la saisie d'opérations, et permet ensuite de les restituer de différentes manières.

On peut visualiser les opérations sous forme de différents tableaux, ou bien de graphiques interactifs, générés grâce à la librairie [Chart.js](https://www.chartjs.org/).

On peut également éditer ou supprimer les opérations saisies.

![separateur](https://up.l3m.in/file/1586716705.png)

## Features

* Système d'inscription grâce à des codes d'invitation
* Connexion/déconnexion
* Système de compte
* Création d'opération
* Édition d'opération
* Suppression d'opération
* Récapitulatifs :
   * Somme de toutes les opérations du mois en cours
   * Somme de toutes les opérations de l'année en cours
   * Somme de toutes les opérations (totale)
* Listing des opérations :
   * Par ordre d'ajout
   * Par mois
   * Par an
   * Par date
   * Par catégorie
   * Par moyen de paiement
* Graphiques des opérations
   * Total des entrées/sorties d'argent par mois
   * Total des entrées/sorties d'argent par mois et par catégories
   * Total des entrées/sorties d'argent par mois et par moyen de paiement
   * Nombre d'opération mensuel par catégorie
* Page "vue d'ensemble" avec des informations pertinentes
* Gestion du profil (édition des informations)
* Interface d'administration pour ajouter/supprimer des codes d'invitation
* Système de notifications et de mémorisation de la page courante (pour les redirections (mal implémenté))
* Design en mobile first (création du style pour tablette/ordinateur après celui du smartphone)

![separateur](https://up.l3m.in/file/1586716705.png)

## Fonctionnement interne

J'avais commencé par organiser le code php suivant le modèle MVC (modèle/vue/controlleur), mais avec la date de rendu qui approchait, il est devenu de plus en plus compliqué de continuer à écrire du code propre, et une partie du site fonctionne grâce à des fonctions écrites un peu n'importe comment.

Le fonctionnement originel suit cette logique :

1. Arrivée sur une page
2. Modification de l'URL par apache (via le fichier `.htaccess`)
3. Invocation de la config (pour la bdd) et du `mainController` via la page `index.php`.
4. Le gros switch dans le `mainController` lit les arguments de l'url écrits par le `.htaccess`, et invoque le modèle correspondant (tous les modèles héritent de la classe `Base`, qui crée une connexion à la bdd et lance quelques fonctions intéressantes).
   * Initialement j'avais créé deux types d'url ; /api/ et /page/. Au final j'ai créé des pages sur des url d'api, parce que je gère différemment les arguments (`page` récupère un seul argument supplémentaire alors que `api` effectue un `explode` et permet donc de récupérer un plus grand nombre d'arguments).
5. Le modèle est chargé (fichiers dans `models/`), et le `mainController` effectue des vérifications ou bien appelle directement une fonction.
6. La fonction effectue sa tambouille (appel à la bdd, appel à une fonction qui génère le code html, redirection vers une autre page, inscription d'un texte dans la variable `$_SESSION['notif']` (la classe `Base.php` vérifie si la variable contient quelque chose, affiche son contenu, et la vide. Cela me permet d'avoir un petit système de notification pratique pour informer les utilisateurs)).
7. Le contenu est affiché, l'utilisateur est content.

![separateur](https://up.l3m.in/file/1586716705.png)

## Quelques précisions

Les tableaux, l'édition/suppression d'opération ainsi que les graphiques sont les dernières choses que j'ai implémenté. Ainsi, leur code est particulièrement illisible. En effet, sachant tout ce qu'il me restait à faire et me rendant bien compte que j'avais eu les yeux plus gros que le ventre, j'ai beaucoup stressé lors de la création de ces différentes parties, ce qui a résulté au final à la création de fonctions mal écrites, à la logique incertaine et qui peuvent potentiellement mener à des failles de sécurité.

![separateur](https://up.l3m.in/file/1586716705.png)

## Puisqu'on parle de sécurité

J'ai cherché à créer une application qui pourrait être utilisée par des vrais gens, ce qui m'a posé de nombreux problèmes dès le début, et a rendu ma progression très lente.

Plusieurs parties du sites sont mal protégées, ou n'ont tout du moins pas bénéficié d'un regard critique sur plusieurs aspects. Je parle notamment des paramètres d'url qui ne sont pas bien vérifiés lorsqu'on demande d'éditer ou de supprimer une opération, et à mes fonctions titanesques et absolument illisibles pour ce qui est de la génération et la mise en forme des données exploitées par `Chart.js` pour générer les graphiques.

Mon idée initiale d'utiliser le javascript pour charger le contenu de la page de manière dynamique n'aura au final pas été créée jusqu'au bout, et l'api ne sert qu'à récupérer les données des graphiques, choisir un mois/une année (persistant dans les variables de session) pour les tableaux détaillés, se connecter/s'inscrire/se déconnecter, ou bien afficher l'interface d'administration.

![separateur](https://up.l3m.in/file/1586716705.png)

## Une interface d'administration ?

Tout à fait. Je suis parti sur une approche "réaliste" de l'application web, et me suis dit qu'il me fallait une sécurité pour éviter que je ne récupère des informations de gens pouvant me mettre dans l'embarras vis-à-vis de la CNIL.

J'ai donc créé un système de code d'activation, afin de pouvoir distribuer le lien un peu autour de moi afin de recevoir des retours (et j'en ai eu). La manipulation de données directement depuis phpmyadmin n'étant pas forcément de mon goût, j'ai préféré me faire une interface d'administration (qui n'autorise que l'utilisateur ayant l'id 1 à y accéder) pour générer de nouvelles clef d'invitation, et pour suivre un peu les comptes qui s'étaient créés (j'ai eu 15 inscrits sur l'application actuellement en ligne, sans me compter moi ou les multicomptes !).

![separateur](https://up.l3m.in/file/1586716705.png)

## Des regrets ?

Pas des regrets, non. Mais je suis déçu de ne pas avoir eu le temps de tout faire ; Je suis déçu de ne pas avoir pu mettre en place le chargement dynamique des données par javascript (avec des preload des pages à chaque fois qu'on hover un lien, ça aurait pu grandement augmenter la rapidité du site (un peu comme le fait le site dev.to)), et j'aurais voulu pouvoir faire des "rapports" qui donnent plus de statistiques sur les opérations. J'aurais également voulu avoir le temps de reprendre mon design (qui est spécial, il faut l'avouer), afin de pouvoir intégrer celui que m'a donné un collègue graphiste/intégrateur.

## Screenshots

Voici quelques images glanées sur le site, et qui permettent de donner une petite idée de ce qu'il permet de faire.

### Tableaux/Graphiques
![img1bis](https://up.l3m.in/file/1586718687.png)

### Quelques pages du site
![img4](https://up.l3m.in/file/1586710344.png)
![img5](https://up.l3m.in/file/1586710397.png)
![img5bis](https://up.l3m.in/file/1586718754.png)
### Interface d'administration
![img6](https://up.l3m.in/file/1586716309.png)
