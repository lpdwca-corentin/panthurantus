<?php

	require_once("model/Base.php");

	// content
	if(isset($_GET['page'])){
		switch ($_GET['page']) {
			case 'login':
				require_once("model/LoginPageManager.php");
				$page = new LoginPageManager("Connexion", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Connexion");
				$page->render();
				break;

			case 'cgu':
				require_once("model/CGUPageManager.php");
				$page = new CGUPageManager("CGU", "Les conditions d'utilisation de l'application Panthurantus.", "Conditions Générales d'Utilisation");
				$page->render();
				break;

			case 'register':
				require_once("model/RegisterPageManager.php");
				$page = new RegisterPageManager("Inscription", "Page d'inscription sur Panthurantus.", "Inscription");
				$page->render();
				break;

			case 'overview':
				require_once("model/OverviewPageManager.php");
				$page = new OverviewPageManager("Vue d'ensemble", "Vue d'ensemble de l'application Panthurantus.", "Vue d'ensemble");
				$page->render();
				break;

			case 'settings':
				require_once("model/SettingsPageManager.php");
				$page = new SettingsPageManager("Paramètres", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Paramètres");
				$page->render();
				break;

			case 'boards':
				require_once("model/BoardsPageManager.php");
				$page = new BoardsPageManager("Tableaux détaillés", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Tableaux détaillés");
				$page->render();
				break;

			case 'graphs':
				require_once("model/GraphsPageManager.php");
				$page = new GraphsPageManager("Graphiques", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Graphiques");
				$page->render();
				break;

			case 'admin':
				require_once("model/AdminPageManager.php");
				$page = new AdminPageManager("Partie administration", "Administration de Panthurantus.", "Partie administration");
				$page->render();
				break;

			case '404':
				require_once("model/Page404PageManager.php");
				$page = new Page404PageManager("Erreur 404", "Page 404.", "Erreur 404");
				$page->render();
				break;

			case '403':
				require_once("model/Page403PageManager.php");
				$page = new Page403PageManager("Erreur 403", "Page 403.", "Erreur 403 - Contenu interdit");
				$page->render();
				break;

			# should never happend
			default:
				require_once("model/HomePageManager.php");
				$page = new HomePageManager("Accueil", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Bienvenue sur Panthurantus");
				$page->render();
				break;
		}
	}
	// api (requets)
	else if(isset($_GET['action'])){
		$action = explode("/", $_GET['action']);
		switch ($action[0]) {
			case 'login':

				if(isset($_POST['email']) &&
					strlen($_POST['email']) > 4
					&& isset($_POST['password'])
					&& strlen($_POST['password']) > 0
				){

					require_once("model/LoginApiManager.php");
					$instance = new LoginApiManager();

					$instance->sanitize($_POST['email'], $_POST['password']);

					$instance->login();
				}
				else{
					$_SESSION['notif'] = ["error", "Erreur inconnue."];
					header('Location:/login/');

				}

				break;

			case 'register':

				if(
					isset($_POST['code'])
					&& isset($_POST['email'])
					&& strlen($_POST['email']) > 4
					&& isset($_POST['password'])
					&& strlen($_POST['password']) > 0
					&& isset($_POST['firstname'])
					&& strlen($_POST['firstname']) > 0
					&& isset($_POST['lastname'])
					&& strlen($_POST['lastname']) > 0
				){

					require_once("model/RegisterApiManager.php");
					$instance = new RegisterApiManager();

					$instance->sanitize($_POST['code'], $_POST['email'], $_POST['password'], $_POST['firstname'], $_POST['lastname']);

					$instance->register();
				}
				else{

					$_SESSION['notif'] = ["error", "Erreur inconnue."];
					header('Location:/register/');

				}
				break;

			case 'logout':

				$_SESSION = array();
				$_SESSION['notif'] = ["success", "Vous êtes maintenant déconnecté."];
				header('Location:/login/');
				break;


			case 'selectYear':
				require_once("model/ChangeYearApiManager.php");
				$instance = new ChangeYearApiManager();
				$instance->updateYear($action[1]);
				break;

			case 'selectMonth':
				require_once("model/ChangeMonthApiManager.php");
				$instance = new ChangeMonthApiManager();
				$instance->updateMonth($action[1], $action[2]);
				break;

			case 'getData':
				require_once("model/GetDataApiManager.php");
				$instance = new GetDataApiManager();
				$instance->getData($action[1], $action[2]);
				break;

			case 'operation':

				if(isset($_POST['time'])
					&& isset($_POST['amount'])
					&& is_numeric($_POST['amount'])
					&& ($_POST['amount'] > 0)
					&& isset($_POST['payment'])
					&& isset($_POST['category'])){

					require_once("model/OperationApiManager.php");
					$instance = new OperationApiManager();

					if(isset($_POST['type']) && $_POST['type'] == "on")

						$instance->createCredit(
							$_POST['time'],
							$_POST['amount'],
							$_POST['payment'],
							$_POST['category']
						);

					else

						$instance->createDebit(
							$_POST['time'],
							$_POST['amount'],
							$_POST['payment'],
							$_POST['category']
						);

				}
				else{
					$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la lecture des champs."];
					header('Location: /overview/');
				}

				break;

			case 'addkeys':
				require_once("model/AdminPageManager.php");
				$request = new AdminPageManager("a", "a", "a");
				$request->add_keys();
				break;

			case 'settings':
				if(
					isset($_POST['email'])
					&& strlen($_POST['email']) > 4
					&& isset($_POST['password'])
					&& strlen($_POST['password']) > 0
					&& isset($_POST['first_name'])
					&& strlen($_POST['first_name']) > 0
					&& isset($_POST['last_name'])
					&& strlen($_POST['last_name']) > 0
				){

					require_once("model/SettingsApiManager.php");
					$request = new SettingsApiManager("a", "a", "a");
					$request->update_settings();

				}
				else{
					print("error");
					$_SESSION['notif'] = ['error', "Une erreur a eu lieu lors de la lecture du formulaire."];
					header('Location: /settings/');
				}

				break;

			case 'edit':
				require_once("model/EditPageManager.php");
				if($action[1] == "submit"){
					if(isset($_POST['type']) && $_POST['type'] == "on")
						$type = True;
					else
						$type = False;
					$request = new EditPageManager("a", "a", "a");
					$request->submit(
							$action[2],
							$type,
							$_POST['time'],
							$_POST['amount'],
							$_POST['payment'],
							$_POST['category']
					);
				}
				else{
					$request = new EditPageManager("Édition", "Édition d'une opération.", "Édition");
					$request->edit($action[1]);
				}
				break;

			case 'delete':
				require_once("model/EditPageManager.php");
				$request = new EditPageManager("a", "a", "a");
				$request->delete($action[1]);
				break;


			default:
				var_dump($_GET);
				//header('Location: /');
				break;
		}
	}
	else{
		// default page
		require_once("model/HomePageManager.php");
		$page = new HomePageManager("Accueil", "Sur Panthurantus vous allez pouvoir noter et suivre vos débits et crédits.", "Bienvenue sur Panthurantus");
		$page->render();
	}
